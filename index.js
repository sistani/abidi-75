const express = require('express');
const app = express();

const DataStore = require('nedb');
const dataFile = new DataStore({filename: './datastores/datafile.db', autoload: true});

const config = {
    port: process.env.PORT || 8000,
    publicDir: __dirname + '/public',
    files: {
        notFoundPage: __dirname + '/public/404.html'
    },
    pages: {
        main: '/abidi'
    }
};

app.use(express.static(__dirname + '/public'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

function setupRoutes() {
    //Homepage
    app.get('/', (req, res) => {
        return res.redirect(config.pages.main);
    });

    //register
    app.post('/api/register', function (req, res) {
        const name = req.body.name;
        if (!name)
            return res.send({
                error: 'no required params'
            });

        const doc = {
            name,
            created: new Date()
        };

        dataFile.insert(doc, function (err, newDoc) {
            if (err) {
                console.error(`datafile error: ${err}`);
                return res.send({
                    error: 'datafile filed'
                });
            }

            console.log(`New user registered: ${name}`);
            return res.send({
                name
            });
        });
    });

    //404
    app.get('*', (req, res) => {
        return res.status(404).sendFile(config.files.notFoundPage);
    });
}

setupRoutes();
app.listen(config.port, () => {
    console.log(`App is listening at http://localhost:${config.port}`)
});
